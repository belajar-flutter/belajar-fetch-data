class Post {
  int id;
  int categoryId;
  String title;
  String seotitle;
  String content;
  String metaDescription;
  String picture;
  String pictureDescription;
  String tag;
  String type;
  String active;
  String headline;
  String comment;
  int hits;
  int createdBy;
  int updatedBy;
  String createdAt;
  String updatedAt;

  Post(
      {this.id,
      this.categoryId,
      this.title,
      this.seotitle,
      this.content,
      this.metaDescription,
      this.picture,
      this.pictureDescription,
      this.tag,
      this.type,
      this.active,
      this.headline,
      this.comment,
      this.hits,
      this.createdBy,
      this.updatedBy,
      this.createdAt,
      this.updatedAt});

  Post.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    categoryId = json['category_id'];
    title = json['title'];
    seotitle = json['seotitle'];
    content = json['content'];
    metaDescription = json['meta_description'];
    picture = json['picture'];
    pictureDescription = json['picture_description'];
    tag = json['tag'];
    type = json['type'];
    active = json['active'];
    headline = json['headline'];
    comment = json['comment'];
    hits = json['hits'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['category_id'] = this.categoryId;
    data['title'] = this.title;
    data['seotitle'] = this.seotitle;
    data['content'] = this.content;
    data['meta_description'] = this.metaDescription;
    data['picture'] = this.picture;
    data['picture_description'] = this.pictureDescription;
    data['tag'] = this.tag;
    data['type'] = this.type;
    data['active'] = this.active;
    data['headline'] = this.headline;
    data['comment'] = this.comment;
    data['hits'] = this.hits;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
